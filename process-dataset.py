import aa2
import sys
import numpy as np

def main(argv):
	if(len(argv) != 5):
		print(">> process-dataset[ERROR]: parameters required (dataset name, x name, y name and y index).")
		return
	else:
		ds_name = argv[1]
		x_name = argv[2]
		y_name = argv[3]
		y_index = int(argv[4])

		aa2.file2arrays(ds_name,x_name,y_name,y_index,",")


if __name__ == "__main__":
	main(sys.argv)
