#!bin/bash

clear

python verify-labels.py ../data/DRF-proc-data/diabetesY.npy
python verify-labels.py ../data/DRF-proc-data/gammaY.npy
python verify-labels.py ../data/DRF-proc-data/isoletY.npy
python verify-labels.py ../data/DRF-proc-data/letterY.npy
python verify-labels.py ../data/DRF-proc-data/madelonY.npy
python verify-labels.py ../data/DRF-proc-data/mfeat-factorsY.npy
python verify-labels.py ../data/DRF-proc-data/mfeat-fourierY.npy
python verify-labels.py ../data/DRF-proc-data/mfeat-karhunenY.npy
python verify-labels.py ../data/DRF-proc-data/mfeat-zernikeY.npy
python verify-labels.py ../data/DRF-proc-data/muskY.npy
python verify-labels.py ../data/DRF-proc-data/optdigitsY.npy
python verify-labels.py ../data/DRF-proc-data/pageblocksY.npy
python verify-labels.py ../data/DRF-proc-data/pendigitsY.npy
python verify-labels.py ../data/DRF-proc-data/segmentY.npy
python verify-labels.py ../data/DRF-proc-data/spambaseY.npy
python verify-labels.py ../data/DRF-proc-data/vehicleY.npy
python verify-labels.py ../data/DRF-proc-data/waveformY.npy
