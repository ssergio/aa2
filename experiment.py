import os
import sys

import numpy as np
import aa2
from sklearn.preprocessing import LabelEncoder

def run_exp(H,K,N,dataset,rootdir):
	#Print some info
	print(">> Processing dataset: "+dataset+"..."),

	fileX = "../data/DRF-proc-data/" + dataset + "X.npy"
	fileY = "../data/DRF-proc-data/" + dataset + "Y.npy"

	#Create folder which will hold the error vectors of ech forest
	subdir = rootdir + "/Error-" + dataset
	os.system("mkdir "+subdir)
	#os.system("chmod 777 "+rootdir)

	#Load datasets
	X, Y, LE = aa2.load_XYarrays(fileX,fileY)

	#Sum of error for later average
	error_sum = 0.0

	for i in range(10):
		#Divide datasets
		DX, DY, DtestX, DtestY = aa2.split_dataset(X,Y,0.667)
		DtX, DtY, DvX, DvY = aa2.split_dataset(DX,DY,0.5)

		#Best forest
		BF = []

		if(H == "GE"):
			#Train the pool of trees
			T, oob = aa2.train_forest(N*3,DtX,DtY,False)
			#Search for the best forest
			BF, otherF = aa2.beam_search_rf("GE",K,N,T,DvX,DvY,[],[],[],[])
			#Compute the final forest error
			err = 1 - aa2.forest_accuracy(BF,T,DtestX,DtestY)
			error_sum = error_sum + err
		elif(H == "VE"):
			#Train the pool of trees
			T, oob = aa2.train_forest(N*3,DtX,DtY,False)
			#Compute the Bem matrix
			Bem = aa2.bin_error_matrix(T,DvX,DvY)
			#Search for the best forest
			BF, otherF = aa2.beam_search_rf("VE",K,N,[],[],[],Bem,[],[],[])
			#Compute the final forest error
			err = 1 - aa2.forest_accuracy(BF,T,DtestX,DtestY)
			error_sum = error_sum + err
		elif(H == "SC"):
			#Train the pool of trees
			T, oob = aa2.train_forest(N*3,DX,DY,True)
			#Compute the OOB matrix
			Oobmat = aa2.oob_mat(T,oob,DX)
			#Search for the best forest
			BF, otherF = aa2.beam_search_rf("SC",K,N,[],[],[],[],Oobmat,DY,LE)
			#Compute the final forest error
			err = 1 - aa2.forest_accuracy(BF,T,DtestX,DtestY)
			error_sum = error_sum + err

		#Compute the progressive error for the forest & save it on file
		outfile = open(subdir+"/ErrForest-"+str(i+1)+".txt","w")
		for j in range(len(BF)):
			sub_bf = BF[0:j+1]
			sub_err = 1 - aa2.forest_accuracy(sub_bf,T,DtestX,DtestY)
			outfile.write(str(sub_err)+"\n")
		outfile.close()
		

	#Give some feedback info to the user
	print("done.")

	#Compute the average error & save it in file
	error_avg = error_sum / 10.0
	perffile = open(rootdir+"/performance.txt","a")
	perffile.write(dataset + ": " + str(error_avg) + "\n")
	perffile.close()
	#os.system("chmod 777 "+rootdir+"/performance.txt")


def main(argv):
	argc = len(argv)
	if(argc != 4):
		print(">> experiment[ERROR]: This script requires 3 input")
		print(">> Correct usage: python experiment.py <H> <K> <N>")
		print(">> H : heuristic to be used, it can be GE, VE or SC")
		print(">> K : size of the \"beam\" used in the seach")
		print(">> N : size of the desired forest")
		return

	if(argv[1] != "GE" and argv[1] != "VE" and argv[1] != "SC"):
		print(">> experiment[ERROR]: First argument must be either GE, VE or SC")
		print(">> Correct usage: python experiment.py <H> <K> <N>")
		print(">> H : heuristic to be used, it can be GE, VE or SC")
		print(">> K : size of the \"beam\" used in the seach")
		print(">> N : size of the desired forest")
		return

	if(argv[2].isdigit() == False):
		print(">> experiment[ERROR]: Second argument must be an integer number")
		print(">> Correct usage: python experiment.py <H> <K> <N>")
		print(">> H : heuristic to be used, it can be GE, VE or SC")
		print(">> K : size of the \"beam\" used in the seach")
		print(">> N : size of the desired forest")
		return

	if(argv[3].isdigit() == False):
		print(">> experiment[ERROR]: Third argument must be an integer number")
		print(">> Correct usage: python experiment.py <H> <K> <N>")
		print(">> H : heuristic to be used, it can be GE, VE or SC")
		print(">> K : size of the \"beam\" used in the seach")
		print(">> N : size of the desired forest")
		return

	kk = int(argv[2])
	nn = int(argv[3])

	if(kk <= 0):
		print(">> experiment[ERROR]: Second argument must be a positive integer number")
		print(">> Correct usage: python experiment.py <H> <K> <N>")
		print(">> H : heuristic to be used, it can be GE, VE or SC")
		print(">> K : size of the \"beam\" used in the seach")
		print(">> N : size of the desired forest")
		return

	if(nn <= 0):
		print(">> experiment[ERROR]: Third argument must be a positive integer number")
		print(">> Correct usage: python experiment.py <H> <K> <N>")
		print(">> H : heuristic to be used, it can be GE, VE or SC")
		print(">> K : size of the \"beam\" used in the seach")
		print(">> N : size of the desired forest")
		return
	#--------------------------------------------------------------------------------------------
	H = argv[1]
	K = kk
	N = nn
	rootdir = H + "-K" + str(K) + "-N" + str(N)
	#os.system("mkdir "+rootdir)

	#perffile = open(rootdir+"/performance.txt","w")
	#perffile.write("+++++ AVERAGE ERROR ON EACH DATASET +++++\n")
	#perffile.close()

	#run_exp(H,K,N,"diabetes",rootdir)
	#run_exp(H,K,N,"gamma",rootdir)
	#run_exp(H,K,N,"isolet",rootdir)
	#run_exp(H,K,N,"letter",rootdir)
	#run_exp(H,K,N,"madelon",rootdir)
	#run_exp(H,K,N,"mfeat-factors",rootdir)
	run_exp(H,K,N,"mfeat-fourier",rootdir)
	run_exp(H,K,N,"mfeat-karhunen",rootdir)
	run_exp(H,K,N,"mfeat-zernike",rootdir)
	run_exp(H,K,N,"musk",rootdir)
	run_exp(H,K,N,"optdigits",rootdir)
	run_exp(H,K,N,"pageblocks",rootdir)
	run_exp(H,K,N,"pendigits",rootdir)
	run_exp(H,K,N,"segment",rootdir)
	run_exp(H,K,N,"spambase",rootdir)
	run_exp(H,K,N,"vehicle",rootdir)
	run_exp(H,K,N,"waveform",rootdir)



if(__name__ == "__main__"):
	main(sys.argv)
