import sys
import numpy as np

def main(argv):
	if(len(argv) != 2):
		print(">> verify-labels[ERROR]: parameters required (label ndarray file name).")
		return
	else:
		Y = np.load(argv[1])
		labels = []
		ys = Y.shape
		for i in range(ys[0]):
			if(Y[i] not in labels):
				labels.append(Y[i])

		tokens = argv[1].split("/")
		setname = tokens[len(tokens)-1]
		print(">> verify-labels[INFO] {} # of labels: {}".format(setname,len(labels)))
		print(">> verify-labels[INFO] {} labels: {}".format(setname,labels))

if __name__ == "__main__":
	main(sys.argv)
