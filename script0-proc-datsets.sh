!bin/bash

clear

python process-dataset.py ../data/DRF-raw-data/diabetes.data ../data/DRF-proc-data/diabetesX ../data/DRF-proc-data/diabetesY 8
python process-dataset.py ../data/DRF-raw-data/gamma.data ../data/DRF-proc-data/gammaX ../data/DRF-proc-data/gammaY 10
python process-dataset.py ../data/DRF-raw-data/isolet.data ../data/DRF-proc-data/isoletX ../data/DRF-proc-data/isoletY 617
python process-dataset.py ../data/DRF-raw-data/letter.data ../data/DRF-proc-data/letterX ../data/DRF-proc-data/letterY 16
python process-dataset.py ../data/DRF-raw-data/madelon.data ../data/DRF-proc-data/madelonX ../data/DRF-proc-data/madelonY 500
python process-dataset.py ../data/DRF-raw-data/mfeat-factors.data ../data/DRF-proc-data/mfeat-factorsX ../data/DRF-proc-data/mfeat-factorsY 216
python process-dataset.py ../data/DRF-raw-data/mfeat-fourier.data ../data/DRF-proc-data/mfeat-fourierX ../data/DRF-proc-data/mfeat-fourierY 76
python process-dataset.py ../data/DRF-raw-data/mfeat-karhunen.data ../data/DRF-proc-data/mfeat-karhunenX ../data/DRF-proc-data/mfeat-karhunenY 64
python process-dataset.py ../data/DRF-raw-data/mfeat-zernike.data ../data/DRF-proc-data/mfeat-zernikeX ../data/DRF-proc-data/mfeat-zernikeY 47
python process-dataset.py ../data/DRF-raw-data/musk.data ../data/DRF-proc-data/muskX ../data/DRF-proc-data/muskY 166
python process-dataset.py ../data/DRF-raw-data/optdigits.data ../data/DRF-proc-data/optdigitsX ../data/DRF-proc-data/optdigitsY 64
python process-dataset.py ../data/DRF-raw-data/pageblocks.data ../data/DRF-proc-data/pageblocksX ../data/DRF-proc-data/pageblocksY 10
python process-dataset.py ../data/DRF-raw-data/pendigits.data ../data/DRF-proc-data/pendigitsX ../data/DRF-proc-data/pendigitsY 16
python process-dataset.py ../data/DRF-raw-data/segment.data ../data/DRF-proc-data/segmentX ../data/DRF-proc-data/segmentY 19
python process-dataset.py ../data/DRF-raw-data/spambase.data ../data/DRF-proc-data/spambaseX ../data/DRF-proc-data/spambaseY 57
python process-dataset.py ../data/DRF-raw-data/vehicle.data ../data/DRF-proc-data/vehicleX ../data/DRF-proc-data/vehicleY 18
python process-dataset.py ../data/DRF-raw-data/waveform.data ../data/DRF-proc-data/waveformX ../data/DRF-proc-data/waveformY 40
