# EXAMPLE ON HOW LOADING DATA AS A NUMPY "ndarray"

# Note:
#
# Pay special attention on the format to extract or read sub-vectors from an ndarray
#
# x = dataset[:,a:b]
#
# where:
#
# x: is the subset of vectors
#
# a: is the first index to be included in the sub-vectors
#
# b: all indexes included in the sub-vectors are less-than "b"
#from aa2 import fileload_data
import numpy as np
import urllib

def fun1():
	fname = "../data/votes.data"
	dt = np.string_
	#fileload_data(fname,0,",",dt)

	l = [["0.12",0.135,0.897],["0.225","1.23","4.675"]]

	print("l: ")
	print(l)

	h = np.asarray(l)
	print("h: ")
	print(h)

	h = h.astype(np.float64,copy=False)
	print("h: ")
	print(h)

def fun2():
	a = np.ndarray(shape=(6,), dtype=np.string_)
	b = np.zeros(shape=(1,),dtype=np.string_)

	print("shape of a: {}".format(a.shape))
	print("shape of b: {}".format(b.shape))

	print("Array a:")
	print(a)
	print("Array b:")
	print(b)

	print("--------------------------------------")
	b = np.append(b,[a[0]],axis=0)
	a = np.delete(a,0,axis=0)
	b = np.append(b,[a[0]],axis=0)
	a = np.delete(a,0,axis=0)

	print("Array a:")
	print(a)
	print("Array b:")
	print(b)

if(__name__ == "__main__"):
	fun2()
