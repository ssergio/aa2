# EXAMPLE ON HOW TO TRAIN A RANDOM FOREST AND CLASSIFY WITH ITS INDIVIDUAL TREES

from sklearn.ensemble import RandomForestClassifier
import numpy as np
import urllib

#Load "pima indians diabetes" dataset
url = "http://goo.gl/j0Rvxq"
raw_data = urllib.urlopen(url)
dataset = np.loadtxt(raw_data, delimiter=",")
X = dataset[0:5,0:8]
Y = dataset[0:5,8]

bx = X[0:1,0:8]
bw = X[1:2,0:8]

print(">> bx = {}".format(bx))
print(">> bw = {}".format(bw))
by = np.concatenate((bx,bw),axis=0)
print(">> by = {}".format(by))
print(">> by.shape = {}".format(by.shape))


#bx = np.concatenate()

#Train Random Forest of 3 trees, with a maximum depth of 2
#rf = RandomForestClassifier(max_depth=2,random_state=0,n_estimators=3)
#rf.fit(X,Y)

#Display some info from each individual tree
#trees = rf.estimators_
#print("len(trees) = {}".format(len(trees)))
#print("Trees parameters:")
#for t in trees:
	#print(">> {}".format(t.get_params(True)))

#Classify the same vector by each tree separately
#print("\n>> Predict a vector with each tree separately:")
#for t in trees:
#	prediction = t.predict(X)
#	ev = error_vector(t,X,Y)
#	print("\n>> Predicted class = {}".format(prediction))
#	print(">> Correct class   = {}".format(Y))
#	print(">> Error vector    = {}".format(ev))












