#
# THIS FILE CONTAINS A SET OF UTILITIES DEVELOPED FOR A MACHINE LEARNING PROJECT.
#
#----------------------------------------------------------------------------------
from random import randint
import numpy as np
import urllib
import math
from sklearn.ensemble import RandomForestClassifier
from sklearn.preprocessing import LabelEncoder
#----------------------------------------------------------------------------------

#----------------------------------------------------------------------------------
# FUNCTION-DESCRIPTION:
#
# This function is for downloading a dataset straight from its url, and returns it
# in the form of two numpy "ndarray"s.
#
# INPUT-PARAMETERS:
#
# -url (string): The URL of the dataset file to be downloaded.
#
# -y_index (integer): Positive integer that is the index of the class label within
#		      the original data vector.
#
# -dlm (string): The delimiter character that separates the components in the origi-
#		 nal data vector.
#
# OUTPUT-PARAMETERS:
#
# -X, Y: Both numpy.ndarray, in X each row is a sample and each column is a feature,
#	 while Y is a vector with the same amount of elements as X of rows.
#
def download_data(url,y_index,dlm):
	#download dataset
	raw_data = urllib.urlopen(url)
	dataset = np.loadtxt(raw_data,delimiter=dlm)
	sr = dataset.shape

	#check if the class label index is within the data dimensions
	if(y_index < 0 or y_index >= sr[1]):
		print(">> Error: y_index is out of bounds, the downloaded dataset vectors has {} features".format(sr[1]))
		return

	#Separate the features and the class label data
	Y = dataset[:,y_index]
	X = np.delete(dataset,y_index,1)

	return X, Y
#----------------------------------------------------------------------------------

#----------------------------------------------------------------------------------
# FUNCTION-DESCRIPTION:
#
# This function is for converting a plain text data file into two ndarray objects,
# one of them for the feature vectors and the other one for the corresponding
# labls. These ndarray objects are saved in disk.
# 
# INPUT-PARAMETERS:
#
# -fname (string): The name of the input dataset file.
# 
# -xname (string): The name for the features ndarray file.
# 
# -yname (string): The name for the labels ndarray file.
# 
# -y_index (integer): Positive integer index of the column that corresponds to the
#                     label, within the input dataset file.
# 
# -dlm (string): Character or string to be considered the separator within the
#                input dataset file.
# 
def file2arrays(fname,xname,yname,y_index,dlm):
	#Verify for a valid delimiter string
	if(dlm == ""):
		print(">> file2arrays[ERROR]: <dlm> cannot be an empty string.")
		return
	
	#Open and load input dataset
	input_data = open(fname,"r")
	dataset = np.loadtxt(input_data,delimiter=dlm,dtype=np.string_)
	input_data.close()

	#Verify if the class label index is within the data dimensions
	d_shape = dataset.shape
	if(y_index < 0 or y_index >= d_shape[1]):
		print(">> file2arrays[ERROR]: <y_index> is out of bounds, the loaded dataset's vectors have {} features".format(sr[1]))
		return

	#Print some info of the dataset
	tokens = fname.split("/")
	print(">> file2arrays[INFO] Dataset name: "+tokens[len(tokens)-1])
	print(">> file2arrays[INFO] # of instances: {0}".format(d_shape[0]))
	print(">> file2arrays[INFO] # of attributes: {0}".format(d_shape[1]-1))


	#Separate the features and the class label data
	Y = dataset[:,y_index]
	X = np.delete(dataset,y_index,1)

	#Convert the features array into 'float64'
	X = X.astype(np.float64,copy=False)

	#Save in disk the extracted ndarrays, i.e., feature and label arrays
	np.save(xname,X)
	np.save(yname,Y)
	print(">> file2arrays[INFO]: feature and label arrays saved.")
#----------------------------------------------------------------------------------

#----------------------------------------------------------------------------------
# FUNCTION-DESCRIPTION:
#
# This is for loading the pair of feature & label arrays at once.
# 
# INPUT-PARAMETERS:
#
# -xfile (string): Name of the features ndarray file.
# 
# -yfile (string): Name of the labels ndarray file.
# 
# OUTPUT-PARAMETERS:
#
# -X (ndarray): Array of feature vectors.
# 
# -Y (ndarray): Array of integer labels corresponding to the vectors in X.
#
# -LE (LabelEncoder): Object which can transform the integer labels into their
#                     original string label value.
#
def load_XYarrays(xfile,yfile):
	X = np.load(xfile)
	Y = np.load(yfile)
	LE = LabelEncoder()
	LE.fit(Y)
	Y = LE.transform(Y)
	return X, Y, LE
#----------------------------------------------------------------------------------

#----------------------------------------------------------------------------------
# FUNCTION-DESCRIPTION:
# This function takes an input dataset & generate two partitions from the input
# dataset. The intersection of these partitions is the empty set, and thei union
# is the original input dataset.
# 
# INPUT-PARAMETERS:
# 
# -full_X (ndarray): Input dataset features array.
# 
# -full_Y (ndarray): Input dataset labels array.
# 
# -percentage_A (float): Relative size of partition A with respect to the input
#                        dataset. Thus, partition B's relative size will be 
#                        (1 - percentage_A).
# OUTPUT-PARAMETERS:
# 
# -XA (ndarray): Partition A features array.
# 
# -YA (ndarray): Partition A labels array.
# 
# -XB (ndarray): Partition B features array.
# 
# -YB (ndarray): Partition B labels array.
# 
def split_dataset(full_X,full_Y,percentage_A):
	#Verify for a valid percentage value
	if(percentage_A < 0 or percentage_A > 1):
		print(">> split_array[ERROR]: percentage_A must be within 0 & 1.")
		return

	#Define the size of each partition
	full_shape = full_X.shape
	size_A = int(full_shape[0] * percentage_A)
	size_B = full_shape[0] - size_A

	#Get a copy of the original dataset
	XA = full_X.copy()
	YA = full_Y.copy()
	XB = np.zeros(shape=(1,full_shape[1]), dtype=np.float64)
	YB = np.zeros(shape=(1,), dtype=np.int)

	#Split the datasets
	for i in range(size_B):
		#Update the num. of rows in partition A
		A_shape = XA.shape 
		max_index = A_shape[0] - 1
		#Generate a random index
		ri = randint(0,max_index)
		#Move the selected vector from partition A to partition B
		XB = np.append(XB,[XA[ri]],axis=0)
		YB = np.append(YB,[YA[ri]],axis=0)
		XA = np.delete(XA,ri,axis=0)
		YA = np.delete(YA,ri,axis=0)

	#Delete from XB & YB the row they were initialized with
	XB = np.delete(XB,0,axis=0)
	YB = np.delete(YB,0,axis=0)

	return XA, YA, XB, YB
#----------------------------------------------------------------------------------

#----------------------------------------------------------------------------------
# FUNCTION-DESCRIPTION:
#
# This function creates a binary vector for a decision tree, it classifies a set of
# samples and its predictions are compared to the actual class label. In the output
# binary vector, 1 means misclassification and 0 otherwise.
#
# INPUT-PARAMETERS:
#
# -d_tree (DecisionTreeClassifier): Individual decision tree classifier.
#
# -x_data (ndarray): Set of numpy ndarrays to be classified.
#
# -y_data (ndarray): Vector of labels, where each element corresponds to a sample in
# 		     the x_data set.
#
# OUTPUT-PARAMETERS:
#
# -err_vec (ndarray): Binary vector of classification errors.
#
def bin_error_vector(d_tree,x_data,y_data):
	#Classify the samples with the decision in tree
	pred_vec = d_tree.predict(x_data)

	#Generate the error vector, where 1 is for misclassified samples and 0 if
	#it was correctly classified
	sy = y_data.shape
	err_vec = np.zeros(shape=(sy[0],),dtype=np.int)
	for i in range(sy[0]):
		#Comparison of predicted and the real class
		if(pred_vec[i] != y_data[i]):
			err_vec[i] = 1

	return err_vec
#----------------------------------------------------------------------------------

#----------------------------------------------------------------------------------
# FUNCTION-DESCRIPTION:
# 
# This function computes the "binary error vector" of every tree contained in T, 
# and it yields a "binary error matrix", in which every row corresponds to a "bev"
# of a tree from T and every column to a data instance in x_data and y_data.
# 
# INPUT-PARAMETERS:
# 
# -T (list of DecisionTreeClassifier): List of trees to compute their "bev".
# 
# -x_data (ndarray): Set of numpy ndarrays to be classified.
# 
# -y_data (ndarray): Vector of labels, where each element corresponds to a sample
# 		     in the x_data set.
# 
# OUTPUT-PARAMETERS:
# 
# -bem (ndarray): "binary error matrix", each row corresponds to the "bev" of a 
# 		  tree in T.
# 
def bin_error_matrix(T,x_data,y_data):
	#Get the amount of trees
	forest_size = len(T)
	#Get the amount of data instances
	sx = x_data.shape

	#Compute the BEV of each tree on x_data & y_data
	bem = np.zeros(shape=(1,sx[0]),dtype=np.int)
	for i in range(forest_size):
		bev = bin_error_vector(T[i],x_data,y_data)
		bem = np.append(bem,[bev],axis=0)

	#Delete the initiliazing zeros vector from the matrix
	bem = np.delete(bem,0,axis=0)

	return bem
#----------------------------------------------------------------------------------

#----------------------------------------------------------------------------------
# FUNCTION-DESCRIPTION:
#
# This function is used for training a random forest, and also enables saving the
# "out of bag" list of instances of each decision tree within the random forest.
# 
# INPUT-PARAMETERS:
# 
# -N (integer): Amount of trees to be trained.
# 
# -dataX (ndarray): Training set's features array.
# 
# -dataY (ndarray): Training set's labels array.
# 
# -save_oob (boolean): If set True, the "out of bag" instances of each decision tree
#                      will be saved.
# 
# OUTPUT-PARAMETERS:
# 
# -forest (list of DecisionTreeClassifier): random forest of trained trees.
# 
# -oob (list of lists): List holding the lists of indexes of the "out of bag"
#                       instances if save_oob is True.
# 
def train_forest(N,dataX,dataY,save_oob):
	#Verify for a valid N & a pair of feature and label arrays with the same
	#amount of instances
	if(N <= 0):
		print(">> train_forest[ERROR]: N must be a positive non-zero integer number.")
		return
	xs = dataX.shape
	ys = dataY.shape
	if(xs[0] != ys[0]):
		print(">> train_forest[ERROR]: dataX and dataY must have the same number of rows.")
		return

	#Train a Random Forest of N trees
	sd = dataX.shape
	max_index = sd[0] - 1
	forest = []
	oob = []

	for i in range(N):
		train_indexes = []
		train_x = np.zeros(shape=(1,sd[1]),dtype=np.float64)
		train_y = np.zeros(shape=(1,),dtype=np.int)

		#Create a "bootstrap" training set
		for j in range(sd[0]):
			ri = randint(0,max_index)
			train_indexes.append(ri)
			train_x = np.append(train_x,[dataX[ri]],axis=0)
			train_y = np.append(train_y,[dataY[ri]],axis=0)

		#Delete the first initiliazer row from train_x & train_y
		train_x = np.delete(train_x,0,axis=0)
		train_y = np.delete(train_y,0,axis=0)

		#Train a single tree rand. forest with the bootstrap training set
		rf = RandomForestClassifier(n_estimators=1,max_features="sqrt",bootstrap=False,random_state=0)
		rf.fit(train_x,train_y)

		#Keep the trained decision tree
		forest.append(rf.estimators_[0])

		#Only if requested, save the "out of bag" instances list of each tree
		if(save_oob):
			oo_bag = []
			for j in range(sd[0]):
				if(j not in train_indexes):
					oo_bag.append(j)
			oob.append(oo_bag)

	return forest, oob
#----------------------------------------------------------------------------------

#----------------------------------------------------------------------------------
# FUNCTION-DESCRIPTION:
# 
# This function is used for classifying a set of instances (dataX), via a Custom
# Random Forest (forest) which is a subset of decision tress of another Random
# Forest (T). Returns the set of the most voted class for each instance in dataX.
# 
# INPUT-PARAMETERS:
# 
# -forest (list of integer): List that contains indexes of decision trees inside 
# 			     the list of trees T.
#
# -T (list of DecisionTreeClassifier): Set of decision trees.
# 
# 
# -dataX (ndarray): Set of instances to be classified. Each row is an instance.
# 
# OUTPUT-PARAMETERS:
# 
# -predictions (ndarray): Set of integers corresponding to the most voted class for
#                         each instance in dataX.
# 
def forest_predict(forest,T,dataX):
	#get the amount of instances to classify in dataX
	sx = dataX.shape

	#Array of final predictions, one for each instance in dataX
	predictions = np.zeros(shape=(sx[0],),dtype=np.int)
	for i in range(sx[0]):
		#Predictions from trees on current instance
		diff_preds = []
		preds = []
		for j in forest:
			#Predict instance i wih tree j
			p = int(T[j].predict([dataX[i,:]]))
			preds.append(p)
			if(p not in diff_preds):
				diff_preds.append(p)

		#Get the most voted class on instance i
		votes = []
		for j in range(len(diff_preds)):
			count = preds.count(diff_preds[j])
			votes.append(count)
		max_count = max(votes)
		index_most_voted = votes.index(max_count)
		most_voted_class = diff_preds[index_most_voted]

		#Save the most voted prediction
		predictions[i] = most_voted_class

	return predictions
#----------------------------------------------------------------------------------

#----------------------------------------------------------------------------------
# FUNCTION-DESCRIPTION:
# 
# This function is for computing the accuracy of a set of trees (forest) that is a
# subset of a bigger set of trees (T), over a set of instances (dataX and dataY).
# 
# INPUT-PARAMETERS:
# 
# -forest (list of integer): List that contains indexes of decision trees inside 
# 			     the list of trees T.
#
# -T (list of DecisionTreeClassifier): Set of decision trees.
# 
# 
# -dataX (ndarray): Set of instances to be classified. Each row is an instance.
# 
# -dataY (ndarray): Set of real labels of feature vectors in dataX.
# 
# OUTPUT-PARAMETERS:
# 
# -f_acc (float): Accuracy performance of forest over dataX and dataY.
# 
def forest_accuracy(forest,T,dataX,dataY):
	#Classify the instances in dataX
	predictions = forest_predict(forest,T,dataX)

	#Get the amount of correct predictions
	sx = dataX.shape
	correct_count = 0
	for i in range(sx[0]):
		if(predictions[i] == dataY[i]):
			correct_count = correct_count + 1

	#Compute the accuracy over all the intances in dataX
	f_acc = float(correct_count) / float(sx[0])

	return f_acc
#----------------------------------------------------------------------------------

#----------------------------------------------------------------------------------
# FUNCTION-DESCRIPTION:
# 
# This function is for calculating the variance of error one tree has compared
# against a set of trees. This function assumes the tree that is to be compared is
# the last one in the list forest. This last tree is compared against all the other
# trees in forest.
# 
# INPUT-PARAMETERS:
# 
# -forest (list of integer): This list holds indexes of trees for which a "BEV" was
#                            computed. Their "BEV"s are in contained in Bem.
# 
# -Bem (ndarray): This array contains the "binary error vectors" of a set of trees,
#                 where forest is a subset of this set.
# 
# OUTPUT-PARAMETERS:
# 
# -ev (float): Is the average error variance of the last tree from forest against
#              all the other trees.
# 
def error_variance(forest,Bem):
	#Get the length of the "binary error vectors"
	sb = Bem.shape
	bev_size = sb[1]

	#Get the size of forest minus the tree is gonna be compared against the forest
	forest_size = len(forest) - 1

	#Get the tree that is going to be compared
	last_tree = forest[len(forest) - 1]

	xor_count = 0
	for i in range(forest_size):
		for j in range(bev_size):
			if(Bem[forest[i],j] != Bem[last_tree,j]):
				xor_count = xor_count + 1

	#Compute the avarage error variance by dividing by forest_size and by
	#dividing by bev_size it is normalized
	ev = float(xor_count) / float(forest_size * bev_size)

	return ev
#----------------------------------------------------------------------------------

#----------------------------------------------------------------------------------
# FUNCTION-DESCRIPTION:
# 
# This function is for searching for a set of size N of decision trees by using a
# "Beam Search" strategy. The heuristic followed in the search is defined by the
# user with parameter H.
# 
# INPUT-PARAMETERS:
# 
# -H (string): Is the heuristic to be followed. Only three heuristics are accepted
# 	       in this implementation, "SC" (strength & correlation), "GE" (genera-
# 	       lization error) and "VE" (variation error).
# 
# -k (integer): This parameter determines the size of the beam list in the search.
# 
# -N (integer): This parameter determines the size of the final built forest.
# 
# -T (list of DecisionTreeClassifier): Set of trees where the search takes place.
# 
# -DvX (ndarray): Validation set's features array.
# 
# -DvY (ndarray): Validation set's labels array.
# 
# -Bem (ndarray): Array holding the "binary error vector" of each tree in T.
# 
# -oobmat (ndarray): Matrix holding the "out of bag" prediction of every tree in T
#                    for each of their "oob" instances.
# 
# -DtY (ndarray): Training set's labels array.
# 
# -LE (LabelEncoder): Object which can transform the integer labels into their
#                     original string label value.
# 		      
# 
# OUTPUT-PARAMETERS:
# 
# -winning_forest (list of integer): First forest of size N found in the
# 				     searching process.
# 
# -L (list of lists): Other forests that were in the "best-k" list when the winning
# 		      forest was found.
# 
def beam_search_rf(H,k,N,T,DvX,DvY,Bem,Oobmat,DtY,LE):
	#Verification of input values and dimensions
	if(H != "SC" and H != "GE" and H != "VE"):
		print(">> beam_search_rf[ERROR]: H must a valid heuristic, i.e., \"SC\", \"GE\" or \"VE\".")
		return
	if(k <= 0):
		print(">> beam_search_rf[ERROR]: k must be a positive non-zero integer.")
		return
	if(N <= 0):
		print(">> beam_search_rf[ERROR]: N must be a positive non-zero integer.")
		return
	if(H == "GE"):
		svx = DvX.shape
		svy = DvY.shape
		if(svx[0] != svy[0]):
			print(">> beam_search_rf[ERROR]: The validation data arrays DvX and DvY must have the same amount of rows.")
			return
		pool_size = len(T)
		if(N > pool_size):
			print(">> beam_search_rf[ERROR]: N must smaller than {} which is the amount of trees available to choose.".format(pool_size))
			return
	if(H == "VE"):
		bem_size = Bem.shape
		pool_size = bem_size[0]
		if(N > bem_size[0]):
			print(">> beam_search_rf[ERROR]: N must smaller than {} which is the amount of trees available to choose.".format(bem_size[0]))
			return
	if(H == "SC"):
		som = Oobmat.shape
		pool_size = som[0]
		if(N > som[0]):
			print(">> beam_search_rf[ERROR]: N must smaller than {} which is the amount of trees available to choose.".format(som[0]))
			return
		sty = DtY.shape
		if(som[1] != sty[0]):
			print(">> beam_search_rf[ERROR]: There must be the same amount of columns in Oobmat as there are rows in DtY.")
			return

	#Select randomly the initial tree t0
	t0 = randint(0,pool_size-1)

	#Give the initial forest t0 a score
	scr0 = -999.0

	#Insert the initial tree t0 in L and its score in S
	L = [[t0]]
	S = [scr0]

	#debug
	#print("Initial L & S:")
	#print("L = {}".format(L))
	#print("S = {}".format(S))
	#print("----------------------------------")
	#ite = 0

	#Start the iterative search
	stop_searching = False
	winning_forest = []
	while(True):
		#debug
		#ite = ite + 1
		#lens = []
		#for i in range(len(L)):
		#	lens.append(len(L[i]))
		#print("Iteration: {}".format(ite))
		#print("Forests' sizes: {} ".format(lens))
		#print("Forests' scores: {}".format(S))

		#Check if there is already a forest of size N in L
		for i in range(len(L)):
			size_forest = len(L[i])
			if(size_forest == N):
				winning_forest = L.pop(i)
				stop_searching = True
				break

		#End the while loop if a forest of size N has been built
		if(stop_searching):
			break

		#Get the first element from L and S
		l = L.pop(0)
		s = S.pop(0)

		#Insert the remaining elements from L in the candidates list
		scores = S[:]
		candidates = L[:]

		#Expand the extracted element l by adding to it every tree from T
		#that is not yet inside l
		for i in range(pool_size):
			if(i not in l):
				score = 0
				l_cand = l + [i]
				if(H == "GE"):
					#Generalization Error heuristic
					score = forest_accuracy(l_cand,T,DvX,DvY)
				elif(H == "VE"):
					#Variation Error heuristic
					score = error_variance(l_cand,Bem)
				elif(H == "SC"):
					#Strength & Correlation heuristic
					score = sc_estimate(l_cand,Oobmat,DtY,LE) * (-1.0)
				scores.append(score)
				candidates.append(l_cand)

		#debug
		#print("candidates = {}".format(candidates))
		#print("scores = {}".format(scores))

		#Select the k best candidates and save them in L and their scores in S
		L = []
		S = []
		for i in range(k):
			best_score = max(scores)
			best_cand_index = scores.index(best_score)
			best_cand = candidates[best_cand_index]

			#Save the candidate and its score
			L.append(best_cand)
			S.append(best_score)

			#Delete "best_cand" from the candidates list
			del scores[best_cand_index]
			del candidates[best_cand_index]

	return winning_forest, L
#----------------------------------------------------------------------------------

#----------------------------------------------------------------------------------
# FUNCTION-DESCRIPTION:
# 
# This function is for computing the "out of bag" prediction of every decidion tree
# for each of its "oob" instances. These predictions are represented int the form of
# a matrix which has a row for each tree and a column for each training instance. If
# an element (i,j) of his matrix has for value -1, this means that the instance (j)
# is not an "oob" instance of the (i) tree. Otherwise, if an element (i,j) has for
# value >= 0, this value is the prediction the tree (i) made for its "oob" instance
# (j).
# 
# INPUT-PARAMETERS:
# 
# -T (list of DecisionTreeClassifier): Set of all existing trees.
# 
# -oob (list of lists): List holding the lists of indexes of the "out of bag"
#                      instances for the trees in T.
# 
# -dataX (ndarray): Set of features vectors that constitute the whole training
#                   dataset. 
# 
# OUTPUT-PARAMETERS:
# 
# -oobmat (ndarray): Matrix holding the "out of bag" prediction of every tree in T
#                    for each of their "oob" instances.
# 
def oob_mat(T,oob,dataX):
	rows = len(T)
	ds = dataX.shape
	cols = ds[0]

	oobmat = np.ndarray(shape=(rows,cols),dtype=np.int)
	oobmat.fill(-1)

	for i in range(rows):
		for j in oob[i]:
			oobmat[i,j] = T[i].predict([dataX[j,:]])

	return oobmat
#----------------------------------------------------------------------------------

#----------------------------------------------------------------------------------
def sc_estimate(forest,oobmat,dataY,LE):
	#Get total amount of classes
	num_cl = len(LE.classes_)

	#Get total amount of training instances & total amount of trees
	oobs = oobmat.shape
	num_in = oobs[1]
	num_tr = oobs[0]

	#Get amount of trees in the forest
	num_ftr = len(forest)

	#Create matrix that will hold "Q" values
	Qmat = np.zeros(shape=(num_cl,num_in),dtype=np.float)

	#Compute the Q value for training instance on every class
	for j in range(num_in):
		#Count of for how many trees that are part of the forest, intance
		#j is "oob"
		oob_count = 0
		for i in forest:
			if(oobmat[i,j] != -1):
				#Increase oob counter
				oob_count = oob_count + 1

				#Increse the vote count of the class predicted
				#in oobmat[i,j]
				Qmat[oobmat[i,j],j] = Qmat[oobmat[i,j],j] + 1

		#Normalize the "oob" classes votes of instance j
		if(oob_count > 0):
			for i in range(num_cl):
				Qmat[i,j] = Qmat[i,j] / oob_count

	#Create a vector that will hold the j-max index of every instance
	jmaxvec = np.zeros(shape=(num_in,),dtype=np.int)

	#Compute the "strength" estimate & the sum necessary for computing the
	#variance of "mr"
	S = 0.0
	mrsum = 0.0
	for j in range(num_in):
		#Get the proportion of votes for the real class Y
		q_y = Qmat[dataY[j],j]

		Qj = np.delete(Qmat[:,j],dataY[j],0)

		#Get the max proportion of votes from the classes excluding Y
		q_jmax = np.amax(Qj)
		
		#Get the j-max index of the current instance and save it in jmaxvec
		jmax_index = np.argmax(Qj)
		if(Qj[jmax_index] == Qmat[jmax_index,j]):
			jmaxvec[j] = jmax_index
		else:
			jmaxvec[j] = jmax_index + 1

		diff = q_y - q_jmax
		S = S + diff
		mrsum = mrsum + pow(diff,2)
	S = S / num_in
	Var_mr = (mrsum / num_in) - pow(S,2)

	#If "strength" is 0, do not bother computing "correlation"
	if(S == 0):
		return 99999.0

	#Compute the estimate of "std. deviation" for every tree in the forest
	sum_std_dev = 0.0
	for i in forest:
		p1sum = 0.0
		p2sum = 0.0
		for j in range(num_in):
			if(oobmat[i,j] > -1):
				if(oobmat[i,j] == dataY[j]):
					p1sum = p1sum + 1
				elif(oobmat[i,j] == jmaxvec[j]):
					p2sum = p2sum + 1
		num_oob	= num_in - np.count_nonzero(oobmat[i,:] == -1)
		p1 = p1sum / num_oob
		p2 = p2sum / num_oob
		std_dev = math.sqrt(p1 + p2 + pow(p1-p2,2))
		sum_std_dev = sum_std_dev + std_dev

	#Avg. "standard deviation" over all the trees in the forest
	Std_Dev = sum_std_dev / num_ftr

	#Compute the correlation estimate
	C = Var_mr / pow(Std_Dev,2)


	#debug
#	print(">> Qmat:")
#	print(Qmat)
#	print(">> jmaxvec:")
#	print(jmaxvec)
#	print("Var(mr): {}".format(Var_mr))
#	print("Std_Dev: {}".format(Std_Dev))
#	print("C: {}".format(C))
#	print("S: {}".format(S))
	return (C / pow(S,2))
#----------------------------------------------------------------------------------
