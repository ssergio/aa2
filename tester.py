import numpy as np
import aa2
from sklearn.preprocessing import LabelEncoder
import os

def fun1():
	X, Y, LE = aa2.load_XYarrays("../data/DRF-proc-data/diabetesX.npy","../data/DRF-proc-data/diabetesY.npy")
	sizeA = 0.05

#	print(">> shapeX: {}  shapeY: {}".format(X.shape,Y.shape))
#	print(">> Size of A: {}*size(X)".format(sizeA))

	XA,YA,XB,YB = aa2.split_dataset(X,Y,sizeA)

#	print(">> shapeX: {}  shapeY: {}".format(X.shape,Y.shape))
#	print(">> shapeXA: {}  shapeYA: {}".format(XA.shape,YA.shape))
#	print(">> shapeXB: {}  shapeYB: {}".format(XB.shape,YB.shape))

	#do the encoding
	forest, oob = aa2.train_forest(4,XA,YA,False)
	for t in forest:
		p = t.predict([XB[0,:]])
		print(p)
		print(LE.inverse_transform(int(p)))
	print("oob: {}".format(oob))

def fun2():
	X, Y, LE = aa2.load_XYarrays("../data/DRF-proc-data/diabetesX.npy","../data/DRF-proc-data/diabetesY.npy")
	sizeA = 0.05
	XA,YA,XB,YB = aa2.split_dataset(X,Y,sizeA)
	forest, oob = aa2.train_forest(4,XA,YA,False)

	crf = [0,1,2]
	dataX = X[0:5,:]

	predictions = aa2.forest_predict(forest,crf,dataX)

	#Printing predictions made by the custom RF
	print("CRF predictions: {}".format(predictions))
	for i in range(len(forest)):
		ind_preds = forest[i].predict(dataX)
		print("T[{}] predictions: {}".format(i,ind_preds))

def fun3():
	X, Y, LE = aa2.load_XYarrays("../data/DRF-proc-data/diabetesX.npy","../data/DRF-proc-data/diabetesY.npy")
	sizeA = 0.05
	XA,YA,XB,YB = aa2.split_dataset(X,Y,sizeA)
	forest, oob = aa2.train_forest(4,XA,YA,False)

	print("Y (real labels): {}".format(Y[0:4]))
	p = forest[0].predict(X[0:4,:])
	print("Y' (pred. labels: {})".format(p))
	bev = aa2.error_vector(forest[0],X[0:4,:],Y[0:4])
	print("Bin. error vector: {}".format(bev))

def fun4():

	X, Y, LE = aa2.load_XYarrays("../data/DRF-proc-data/diabetesX.npy","../data/DRF-proc-data/diabetesY.npy")
	DX, DY, DtestX, DtestY = aa2.split_dataset(X,Y,0.67)
	DtX, DtY, DvX, DvY = aa2.split_dataset(DX,DY,0.5)
	
	print("Shapes: X={} Y={}".format(X.shape,Y.shape))
	print("Shapes: DtestX={} DtestY={}".format(DtestX.shape,DtestY.shape))
	print("Shapes: DtX={} DtY={}".format(DtX.shape,DtY.shape))
	print("Shapes: DvX={} DvY={}".format(DvX.shape,DvY.shape))

	T, oob = aa2.train_forest(7,DtX,DtY,False)
	H = "GE"
	k = 2
	N = 4
	bsf, L = aa2.beam_search_rf(H,k,N,T,[],[],[],DvX,DvY,[])

	print("Beam Search Forest: {}".format(bsf))
	print("Other forests:")
	print(L)
	

#Testing Gen. Error beam search
def fun5():
	#Load dataset
	X, Y, LE = aa2.load_XYarrays("../data/DRF-proc-data/diabetesX.npy","../data/DRF-proc-data/diabetesY.npy")

	#Divide datasets
	DX, DY, DtestX, DtestY = aa2.split_dataset(X,Y,0.667)
	DtX, DtY, DvX, DvY = aa2.split_dataset(DX,DY,0.5)
	
	#Print info about datasets
	print("Shapes: X={} Y={}".format(X.shape,Y.shape))
	print("Shapes: DtestX={} DtestY={}".format(DtestX.shape,DtestY.shape))
	print("Shapes: DtX={} DtY={}".format(DtX.shape,DtY.shape))
	print("Shapes: DvX={} DvY={}".format(DvX.shape,DvY.shape))

	#Train the pool of trees
	T, oob = aa2.train_forest(60,DtX,DtY,False)

	#Search for the best forest
	bestF, otherF = aa2.beam_search_rf("GE",6,20,T,DvX,DvY,[],[],[],[])

	#Print the winning forest & its test performance
	print("Winning Forest of size N = 20:")
	print(bestF)
	test_acc = aa2.forest_accuracy(bestF,T,DtestX,DtestY)
	print("Test accuracy: {}".format(test_acc))

#Testing Variation Error beam search
def fun6():
	#Load dataset
	X, Y, LE = aa2.load_XYarrays("../data/DRF-proc-data/diabetesX.npy","../data/DRF-proc-data/diabetesY.npy")

	#Divide datasets
	DX, DY, DtestX, DtestY = aa2.split_dataset(X,Y,0.667)
	DtX, DtY, DvX, DvY = aa2.split_dataset(DX,DY,0.5)
	
	#Print info about datasets
	print("Shapes: X={} Y={}".format(X.shape,Y.shape))
	print("Shapes: DtestX={} DtestY={}".format(DtestX.shape,DtestY.shape))
	print("Shapes: DtX={} DtY={}".format(DtX.shape,DtY.shape))
	print("Shapes: DvX={} DvY={}".format(DvX.shape,DvY.shape))

	#Train the pool of trees
	T, oob = aa2.train_forest(60,DtX,DtY,False)

	#Compute the Bem matrix
	Bem = aa2.bin_error_matrix(T,DvX,DvY)

	#Search for the best forest
	bestF, otherF = aa2.beam_search_rf("VE",6,20,[],[],[],Bem,[],[],[])

	#Print the winning forest & its test performance
	print("Winning Forest of size N = 20:")
	print(bestF)
	test_acc = aa2.forest_accuracy(bestF,T,DtestX,DtestY)
	print("Test accuracy: {}".format(test_acc))

#Testing Strength & Correlation beam search
def fun7():
	#Load dataset
	X, Y, LE = aa2.load_XYarrays("../data/DRF-proc-data/diabetesX.npy","../data/DRF-proc-data/diabetesY.npy")

	#Divide datasets
	DtX, DtY, DtestX, DtestY = aa2.split_dataset(X,Y,0.667)
	
	#Print info about datasets
	print("Shapes: X={} Y={}".format(X.shape,Y.shape))
	print("Shapes: DtestX={} DtestY={}".format(DtestX.shape,DtestY.shape))
	print("Shapes: DtX={} DtY={}".format(DtX.shape,DtY.shape))

	#Train the pool of trees
	T, oob = aa2.train_forest(60,DtX,DtY,True)

	#Compute the OOB matrix
	Oobmat = aa2.oob_mat(T,oob,DtX)
	print("Shape: Oobmat={}".format(Oobmat.shape))

	#Search for the best forest
	bestF, otherF = aa2.beam_search_rf("SC",6,20,[],[],[],[],Oobmat,DtY,LE)

	#Print the winning forest & its test performance
	print("Winning Forest of size N = 20:")
	print(bestF)
	test_acc = aa2.forest_accuracy(bestF,T,DtestX,DtestY)
	print("Test accuracy: {}".format(test_acc))

def fun8():
	L = [1,2,3,4,5,8]
	ff = open("dudadu.txt","a")
	for l in L:
		ff.write(str(l)+"\n")
	ff.close()


if(__name__ == "__main__"):
	fun8()



